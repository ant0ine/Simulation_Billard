#!/usr/bin/python3
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine & aymeric
@date = Wed 07/05/2016
'''

from tkinter import *
from tkinter.messagebox import *

import math
import time
import sys
sys.path.append("./src")
from Plateau import *
from Boule   import *
from constantes import *
#from UI import *
from Players import *




class UIJeu(Tk):
    def __init__(self, nb_joueur = 2):
        Tk.__init__(self)
        #self.parent = parent # Utile pour montrer/masquer des groupes de widgets, les redessiner à l'écran ou les détruire quand la fenêtre est fermée.
        self.title("JOUONS AU BILLARD !!!")
        self.tapis = Plateau(L, H)
        
        self.liste_joueur = ListPlayers()
        self.initializePlayers(nb_joueur)
        
        self.fleche_existe = 0
        #self.boules_existent = 0
        
        self.initializeGame()
        self.initializeUI()
        self.aimTarget()

        self.updateUI()

        #init player
    
    def initializeGame(self):
        ##### CRÉATION DES BOULES #####
        self.tapis.ajouterBoules(xr, yr, couleur = 'Red')
        self.tapis.ajouterBoules(xb, yb, couleur = 'White')
        self.tapis.ajouterBoules(xj, yj, couleur = 'Yellow')
          
    def initializePlayers(self, k):
        """
        Création de la liste des joueurs
        """
        for j in range(k-1):
            self.liste_joueur.addPlayer()
        self.liste_joueur.makePlayerReady()
        self.liste_joueur.firstPlayer()
    
    def initializeUI(self):
        """
        Prépare l'UI du jeu
        """
        self.grid()
        
        # Menubar 
        ### Menu fichier
        menubar = Menu(self)
        menu_fichier = Menu(menubar)#, tearoff=0)
        menu_fichier.add_separator()
        menu_fichier.add_command(label = "Quitter", command = self.quit)
        menubar.add_cascade(label="Fichier", menu = menu_fichier)
        self.config(menu=menubar)
        
        # Tapis vert
        self.can = Canvas(self, width=xmax, height=ymax, bg='dark green', 
                            highlightbackground = 'brown')
        self.can.grid(row=1, column=0, rowspan=4)
        
        # Commentaire de jeu
        self.comm = Label(self, text ="Joueur %i joue. Il s'agit de : %s. Numéro équipe / boule : %i"%(
                                self.liste_joueur.currentPlayer().numero, 
                                self.liste_joueur.currentPlayer().nom,
                                self.liste_joueur.currentPlayer().numero_boule),
                        fg="black", bg="white")
        self.comm.grid(row=0, column=0)
        
        #Nombre de joueurs
        self.joueurs = Label(self, text ="Il y a %i joueurs"%(Player.nombre_joueur_cree),
                        fg="black")
        self.joueurs.grid(row=0, column=1)
        
        # Curseur angle de tir      
        choix_angle = Label(self, text ="Choix de l'angle entre 0 et 360 degrés")
        choix_angle.grid(row=1, column=1)
        self.angle2 = IntVar()
        s = Scale(self, orient='horizontal', from_=0, to=360, 
                    variable = self.angle2, activebackground='blue')
        s.grid(row=2, column=1)
        
        # Curseur Choix puissance
        self.force = IntVar()
        choix_puissance = Scale(self, orient='vertical', from_=0, to=100, 
                                resolution=1, tickinterval=25, length=200, 
                                label='Puissance', activebackground='red', 
                                variable = self.force)
        choix_puissance.grid(row=3, column=1)
        
        # Bouton Tirer
        bouton_tirer = Button(self, text='Tirer', 
                                activebackground='green', command=self.shoot)
        bouton_tirer.grid(row=4, column=1)
        
        # Placement des boules
        self.drawBalls()
        
    def drawBalls(self):
        """
        Créer sur le canva les boules
        """
        self.boule_rouge = self.can.create_oval(self.tapis.boules[0].x-rayon, 
                                                self.tapis.boules[0].y-rayon, 
                                                self.tapis.boules[0].x+rayon, 
                                                self.tapis.boules[0].y+rayon, 
                                                fill=self.tapis.boules[0].couleur)
        self.boule_blanche = self.can.create_oval(self.tapis.boules[1].x-rayon, 
                                                self.tapis.boules[1].y-rayon, 
                                                self.tapis.boules[1].x+rayon, 
                                                self.tapis.boules[1].y+rayon, 
                                                fill=self.tapis.boules[1].couleur)
        self.boule_jaune = self.can.create_oval(self.tapis.boules[2].x-rayon, 
                                                self.tapis.boules[2].y-rayon, 
                                                self.tapis.boules[2].x+rayon, 
                                                self.tapis.boules[2].y+rayon, 
                                                fill=self.tapis.boules[2].couleur)

        self.liste_boule_can = [self.boule_rouge, self.boule_blanche, self.boule_jaune]
    
    def deleteBalls(self):
        """
        Supprime du canva les boules
        """
        self.can.delete(self.boule_rouge)
        self.can.delete(self.boule_jaune)
        self.can.delete(self.boule_blanche)
        
    def aimTarget(self):
        """
        Gestion de l'orientation de la flèche sur la boule du joueur
        """
        # x, y position de la boule du joueur dont c'est le tour

        numero_boule = self.liste_joueur.currentPlayer().numero_boule
        x = self.tapis.boules[numero_boule].x
        y = self.tapis.boules[numero_boule].y
        puissance = self.force.get()
        self.angle = self.angle2.get()*math.pi/180
        
        if self.fleche_existe == 1:
            self.can.delete(self.viseur)
        
        self.viseur = self.can.create_line(x, y, x+(30+puissance)*math.cos(self.angle), 
                                            y-(30+puissance)*math.sin(self.angle), 
                                            width = 2, arrow=FIRST)
        self.fleche_existe = 1
        
        self.after(50, self.aimTarget)

    
    def updateComment(self):
        """
        Met à jour l'encadré de couleur pour savoir l'équipe qui doit jouer
        """
        numero_boule =  self.liste_joueur.currentPlayer().numero_boule
        if numero_boule == 1: color = "white"
        else : color = "yellow"
        
        # On delete
        self.can.delete(self.comm)
        
        # On crée à nouveau
        self.comm = Label(self, text ="Joueur %i joue. Il s'agit de : %s. \
                                Numéro équipe : %i. \
                                Équipe 1 : %i pts. Équipe 2 : %i pts. "%(
                                self.liste_joueur.currentPlayer().numero, 
                                self.liste_joueur.currentPlayer().nom,
                                self.liste_joueur.currentPlayer().numero_boule,
                                self.tapis.points[0],
                                self.tapis.points[1]),
                        fg="black", bg=color)
        self.comm.grid(row=0, column=0)
        
    def shoot(self):
        """
        Fonction appelée lorsque le joueur veut Tirer
        -> Met à jour les position des boules
        -> Passe au joueur suivant
        -> Met à jour les Commentaires
        -> Change la fleche de boule
        """
        numero_boule =  self.liste_joueur.currentPlayer().numero_boule
        boule = self.tapis.boules[numero_boule]
        
        self.can.delete(self.viseur)
        self.fleche_existe = 0
        
        self.tapis.frapperBoule(boule, self.angle, self.force.get()/3)

        self.liste_joueur.nextPlayer()
        self.updateComment()
        print("ajouter pts")
        self.tapis.ajouterPoint(numero_boule)
        print("on reset")
        print(self.tapis.contact)
        self.tapis.tourSuivant(numero_boule)
        print(self.tapis.contact)
        # self.updateUI2()
        self.updateUI2()
    

    def updateUI(self):
        """
        Première tentative de gestion du mouvement en supprimant les boules et 
        les faisant réapparaitre
        """     
        numero_boule =  self.liste_joueur.currentPlayer().numero_boule
        print("UI1")
        if self.tapis.testMouvement():
            self.deleteBalls()
            self.tapis.mettreAJour(numero_boule)
            self.drawBalls()
            self.after(40, self.updateUI)


    def updateUI2(self):
        """
        Deuxième version de updateUI qui utilise la méthode move() de Tkinter
        """
        numero_boule =  self.liste_joueur.currentPlayer().numero_boule
        print("UI2")
        obj0 = self.tapis.boules[0]
        obj1 = self.tapis.boules[1]
        obj2 = self.tapis.boules[2]

        dx0 = obj0.x
        dy0 = obj0.y
        dx1 = obj1.x
        dy1 = obj1.y
        dx2 = obj2.x
        dy2 = obj2.y

        # On met à jour le tapis
        self.tapis.mettreAJour(numero_boule)

        # On récupère les nouveaux objects [est ce vraiment nécessaire ?]
        obj0 = self.tapis.boules[0]
        obj1 = self.tapis.boules[1]
        obj2 = self.tapis.boules[2]

        # Déplacement selon x et y de chacune des boules
        dx0 = obj0.x - dx0
        dy0 = obj0.y - dy0
        dx1 = obj1.x - dx1
        dy1 = obj1.y - dy1
        dx2 = obj2.x - dx2
        dy2 = obj2.y - dy2

        # On bouge les éléments graphiques
        self.can.move(self.liste_boule_can[0], dx0, dy0)
        self.can.move(self.liste_boule_can[1], dx1, dy1)
        self.can.move(self.liste_boule_can[2], dx2, dy2)

        # On met à jour l'affichage
        self.update()
        self.after(35, self.updateUI2)

    

if __name__ == '__main__':
    #app2 = UIJeu(None)
    app2 = UIJeu(3)
    app2.mainloop()
