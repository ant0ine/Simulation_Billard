Simulation de billard

L'objectif de ce projet est de simuler un jeu de billard français. Le jeu sera composé d'un tapis de billard sur lequel seront posées plusieurs boules (3). Les trois boules sont une boule blanche, une boule jaune et une boule rouge. Chaque joueur se voit attribué une des deux boules (blanche ou jaune).

Éxécuter le jeu :
- Lancer le script UIJeu.py.
Par défaut, le jeu est configuré pour jouer en 2v1 avec le joueur de la deuxième équipe qui va jouer deux fois dans un tour complet.

- Le script UI.py
Choisir le nombre de joueur dans le champ en bas et valider par le bouton sur la droite. Lancer le jeu avec le bouton "Jouer"
La deuxième fenêtre ne s'initialise par complètement lorsque l'on fait appel à cet interface pour lancer le jeu mais le nombre de joueur est correctement mis.


Retrouver le jeu :
- Projet disponible sur https://git.framasoft.org/ant0ine/Simulation_Billard

Les fichiers du jeu :
- Les deux fichiers dans ce dossier sont les fichiers de l'interface Tkinter
- Dans le dossier src/ on retrouve tout le programme d'arrière plan
- Dans le dossie tests_mouvement on retrouve les trois fichiers de tests pour corriger le mouvement des boules et leur rebond. 

Consigne du projet :
- Programme à réaliser :
Afin de rester le plus général possible, la simulation devra permettre de prendre en compte un nombre quelconque de boules. Elle sera donc consacrée au billard français mais s'adaptera sans problème à d'autres types de billard. Le mouvement de chaque boule sera régi par une équation différentielle qui sera intégrée numériquement par la méthode de votre choix (Euler, RK2, RK4 par exemple).
Le programme devra donc parcourir l'ensemble des boules, intégrer leur trajectoire sur un petit intervalle de temps, et recommencer tant qu'il reste des boules en mouvement.
Concernant les équations définissant le mouvement des boules, il faut écrire les équations dynamiques, et pour simplifier le problème, on pourra se restreindre à de la dynamique du point. Il ne sera alors pas possible de donner des effets, mais les équations seront beaucoup plus simples ! Il faudra également prendre en compte les chocs sur les bandes ou sur les autres boules.

- Évolutions du programme
Une fois la simulation de base mise en place, il est possible d'ajouter quelques améliorations simples. On peut par exemple considérer que différents objets (plus uniquement des boules) sont présents sur le tapis. Il pourra par exemple s'agir d'obstacles (objets immobiles).
On pourra également simuler une compétition de billard artistique : on ajoute sur le tapis des boules ou des verres en cristal, et l'objectif est de toucher les deux boules normales sans casser les autres.
Il est possible d'adapter la simulation à d'autres jeux de billard (billard américain, pool, snooker, ...)
