# -*- coding: utf-8 -*-
"""
Created on Thu May 19 21:26:01 2016

@author: Aymeric
"""

from tkinter import * 
from math import sqrt

#Rajouté pour les collisons entre boules
def produitScalaire(vect1,vect2):
    """
    Produit scalaire canonique dans Rn
    """
    n=len(vect1)
    if n != len(vect2):
        return "Pb de dimensions"
    else:
        result = 0
        for i in range(n):
            result += vect1[i]*vect2[i]
        return result

def vectNormalTangentiel(xr,yr,xj,yj):
    """
    Retourne les vecteurs g et n normalisés
    """

    n_aux = [xr-xj, yr-yj]
    norme = sqrt(produitScalaire(n_aux,n_aux))

    g_aux = [yj-yr, xr-xj]

    n = [x * (1/norme) for x in n_aux]
    g = [x * (1/norme) for x in g_aux]
    print("norme de n=",sqrt(produitScalaire(n,n)))
    return(n,g)

def projection2(xr,yr,xj,yj,dxr,dyr,dxj,dyj):
    """
    Fait la projection des vitesses sur n et g
    """
    (n,g) = vectNormalTangentiel(xr,yr,xj,yj)
    
    n1_aux = produitScalaire(n,[dxr,dyr])
    g1_aux = produitScalaire(g,[dxr,dyr])
    
    n2_aux = produitScalaire(n,[dxj,dyj])
    g2_aux = produitScalaire(g,[dxj,dyj])
    
    n1 = [x * n1_aux for x in n] #vitesse de B1 projetée sur n
    g1 = [x * g1_aux for x in g] #vitesse de B1 projetée sur g
    
    n2 = [x * n2_aux for x in n]
    g2 = [x * g2_aux for x in g]
    
    return (n1,g1,n2,g2)




i = 0

fenetre = Tk()
dxr=6
dyr=0
dxj=2
dyj=0

xr0=395
yr0=195
xr1=405
yr1=205
xj0=295
yj0=195
xj1=305
yj1=205
xr=xr0+(xr1-xr0)/2
yr=yr0+(yr1-yr0)/2
xj=xj0+(xj1-xj0)/2
yj=yj0+(yj1-yj0)/2
can = Canvas(fenetre, width=800, height=400, bg='dark green', highlightbackground = 'brown')
can.grid(row=1, column=0, rowspan=4)
boule = can.create_oval(390, 190, 410, 210, fill="red")
boule2 = can.create_oval(290, 190, 310, 210, fill="yellow")
while True:
    i+=1
    #print(i)
    if xr0 <=0+5 or xr1 >= 800-5:
        dxr=-dxr
    if xj0 <=0+5 or xj1 >= 800-5:
        dxj=-dxj
    if yr0 <=0+5 or yr1 >= 400-5:
        dyr=-dyr
    if yj0 <=0+5 or yj1 >= 400-5:
        dyj=-dyj
    if sqrt((yj-yr)**2+(xj-xr)**2) <= 20 + 5:
        n1, g1, n2, g2 = projection2(xr,yr,xj,yj,dxr,dyr,dxj,dyj)
        #print("n1=",n1,"g1=",g1,"n2=",n2,"g2=",g2)
        n1, g1, n2, g2 = n2, g1, n1, g2
        dxr = n1[0] + g1[0]
        dyr = n1[1] + g1[1]
        dxj = n2[0] + g2[0]
        dyj = n2[1] + g2[1]
    can.move(boule, dxr, dyr) 
    can.move(boule2, dxj, dyj)
    xr0+=dxr
    yr0+=dyr
    xr1+=dxr
    yr1+=dyr
    xj0+=dxj
    yj0+=dyj
    xj1+=dxj
    yj1+=dyj
    xr=xr0+(xr1-xr0)/2
    yr=yr0+(yr1-yr0)/2
    xj=xj0+(xj1-xj0)/2
    yj=yj0+(yj1-yj0)/2
    fenetre.update()
    fenetre.after(50) 
fenetre.mainloop()
